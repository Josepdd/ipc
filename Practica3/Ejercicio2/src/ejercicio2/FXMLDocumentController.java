/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

/**
 *
 * @author micomuo
 */
public class FXMLDocumentController {
    
    @FXML
    private Slider valores;
    @FXML
    private Label rate;
    @FXML
    private TextField input;
    @FXML
    private TextField output;
    @FXML
    private Button convert;
    @FXML
    private Button clear;
    @FXML
    private CheckBox automatic;
    
    @FXML
    public void initialize() {
        input.setText("0.0");
        output.setText("0.0");
        rate.textProperty().bind(Bindings.format("%.2f",valores.valueProperty()));
        convert.disableProperty().bind(automatic.selectedProperty());
        
    }

    public void convert() {
        double aux = 0;
        aux = valores.getValue() * Double.parseDouble(input.getText().replace(',', '.'));
        output.setText(String.format("%.2f", aux));
    }
    
    public void clear() {
        output.setText("0.0");
        input.setText("0.0");
    }
    
    public void automaticMode() {
        DoubleProperty res = new SimpleDoubleProperty(0.0);
        if(automatic.isSelected()) {
            
            input.textProperty().addListener(new ChangeListener<String>(){
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    res.setValue(valores.getValue() * Double.parseDouble(newValue.replace(',','.')));
                }
                
            });
            
            valores.valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    res.setValue(newValue.doubleValue() * Double.parseDouble(input.getText().replace(',','.')));
                }
            });
            
            output.textProperty().bind(Bindings.format("%.2f", res));
        } else if(!automatic.isSelected()) {
            output.textProperty().unbind();
        }
    }
    
}
